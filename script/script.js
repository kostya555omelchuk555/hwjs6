function createNewUser() {
  let firstName = prompt("Введіть своє ім'я:");
  let lastName = prompt("Введіть своє прізвище:");
  let birthdayStr = prompt("Введіть свою дату народження у форматі dd.mm.yyyy:");
  let [day, month, year] = birthdayStr.split(".");
  let birthday = new Date(year, month - 1, day);

  let newUser = {
    firstName,
    lastName,
    birthday,
    getAge() {
      let today = new Date();
      let age = today.getFullYear() - this.birthday.getFullYear();
      let monthDiff = today.getMonth() - this.birthday.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < this.birthday.getDate())) {
        age--;
      }
      return age;
    },
    getPassword() {
      return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
    },
    getLogin() {
      return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    },
  };

  return newUser;
}

let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());
 